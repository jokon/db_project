# Design of an Airport Data Base using phpMyAdmin, MySQL. 

This project was implemented for the Data Bases course, taught by Prof. Nikolaos Avouris in the University of Patras, Greece. It focuses on the creation of an Airport Data Base, taking into account the Arrivals, Departures, Gates as well as the available parking space. 

## Installation
 
This project can be run using [phpMyAdmin](https://www.phpmyadmin.net/).


## Analysis and Design of the Airport Data Base

#### Entity Relationship Diagram (ERD) 

<img src="./images/ERD.png" width="100%">

As shown in the above image, the ERD consists of the following:

- __Entities:__
 
  1) The _Company_ that cooperates with the Airport
 
  2) The _employee_ 
  
  3) The _aircraft_
  
  4) The _flight_

  5) The _arrival_
  
  6) The _departure_
  
  7) The _runway_
  
  8) The _gate_
  
  9) The _apron's seat_
   
__note:__ Every entity has its attributes.


- __Relationships:__ 

  1) The Company hires the employees.
  
  2) The Company possesses or hires aircrafts.
  
  3) The main service that the company provides customers with is the realization of flights.
  
  4) The flights are realized by the employees by their using the aircrafts.
  
  5) Every flight is related to a gate (it is in the equivalent terminal of the airplane) and an   apron_seat that is defined by the gate.  
  
  6) Every flight realizes either arrival or departure.  
  
  7) The runway is used by an aircraft either for arrival or for departure.

#### Extended Entity Relationship Diagram (EERD)

<img src="./images/E-ERD.png" width="100%">

The __specialization relationships__ that are added are the following:

1) The _employee_ may be exclusively a _pilot_ or a _flight attendant_ (disjoint rule).

2) The _Company_ may be an _airline_ or an _aircharter_. An _airline_ is a company that realizes scheduled flights and which allows every customer to rent a single seat, whereas an _aircharter_ is a company that allows the customer to rent the whole aircraft and define the departure and arrival time. It is assumed that the above two services can be offered by the same company(overlap rule).

3) The _Company_ can transport either a _passenger_ or a _freight_(overlap rule).

4) The _flight_ can be _domestic_, _international_ or _intercontinental_ (disjoint rule).

__Basic Assumptions:__

1) The airport, whose Data Base is designed, is of medium size. This means that there is one terminal building, two runways, ten gates and ten parking seats.

2) The parkings last a short period of time, that is to say, the aircrafts are parked for less than two days. As a result, _hangars_ have been omitted and the apron's seats have been regarded as the parking seats.

3) There can only be one arrival-departure during a specific period of time. 

4) _Non-direct flights_ are not included in this DB. Thus, the entity _route_ has not been defined.

5) Private citizens' aircrafts have not been included in the DB.

6) History of departures and arrivals is related to a specific date.

#### Relational Model

<img src="./images/Relational Model.png" width="80%"> 
<img src="./images/Relational Model 1.png" width="80%"> 

In the above images, the transformation of the EERD into a Relational Model is realized. The DB is represented by a collection of tables with discrete names. Each table is called a relation.

#### License 

Copyright © 2016 Ioanna Kontou

This program is Free Software: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the
[GNU General Public License](https://www.gnu.org/licenses/gpl.html) as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.






