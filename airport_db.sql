/*  
    db_project
    Copyright (C) 2016 Ioanna Kontou

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

CREATE TABLE IF NOT EXISTS `company`(
	`id` INT NOT NULL,
	`name` VARCHAR(255),
	`IATA` VARCHAR(255) ,
	`ICAO` VARCHAR(255) ,
	`callsign` VARCHAR(255) ,
	`country` VARCHAR(255) ,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `airline`(
	`com_id` INT NOT NULL,
	PRIMARY KEY (`com_id`)
);

CREATE TABLE IF NOT EXISTS `aircharter`(
	`comp_id` INT NOT NULL,
	PRIMARY KEY (`comp_id`)
);

CREATE TABLE IF NOT EXISTS `freight_company`(
	`comp_id` INT NOT NULL,
	PRIMARY KEY (`comp_id`)
);

CREATE TABLE IF NOT EXISTS `passenger_company`(
	`comp_id` INT NOT NULL,
	PRIMARY KEY (`comp_id`)
);

CREATE TABLE IF NOT EXISTS `employee`(
	`id` INT NOT NULL,
	`firstname` VARCHAR(255) ,
	`lastname` VARCHAR(255) ,
	`birthdate` DATE ,
	`SEX` VARCHAR(255) ,
	`salary` FLOAT ,
	`comp_id` INT ,
	
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `pilot`(
	`id` INT NOT NULL,
	`license_num` VARCHAR(255),
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `flight_attendant`(
	`id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `aircraft`(
	`id` INT NOT NULL,
	`manufacturer` VARCHAR(255) ,
	`model` VARCHAR(255) ,
	`MTOW` FLOAT ,
	`approach speed` INT ,
	`wingspan` FLOAT ,
	`FAA code` VARCHAR(255) ,
	`tail height` FLOAT ,
	`ARC` VARCHAR(255) ,
	`comp_id` INT ,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `passenger_aircraft`(
	`id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `freight_aircraft`(
	`id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `flight`(
	`id` INT NOT NULL,
	`name` VARCHAR(255) ,
	`depart_airport` VARCHAR(255) ,
	`arrival_airport` VARCHAR(255) ,
	`date` DATE ,
	`comp_id` INT ,
	`aircraft_id` INT ,
	`duration` TIME,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `domestic_flight`(
	`id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `international_flight`(
	`id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `intercontinental_flight`(
	`id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `arrival`(
	`id` INT NOT NULL,
	`date_of_arrival` DATETIME,
	`ETA` TIME ,
	`STA` TIME ,
	`status` VARCHAR(255) ,
	`flight_id` INT ,
	`runway_id` INT ,
	`orientation` VARCHAR(255) ,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `departure`(
	`id` INT NOT NULL,
	`date_of_departure` DATETIME,
	`STD` TIME ,
	`status` VARCHAR(255) ,
	`flight_id` INT ,
	`runway_id` INT ,
	`orientation` VARCHAR(255) ,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `runway`(
	`id` INT NOT NULL,
	`name` VARCHAR(255) ,
	`width` FLOAT ,
	`lenght` FLOAT ,
	`max_weight` FLOAT ,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `gate`(
	`id` INT NOT NULL,
	`name` VARCHAR(255) ,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `apron_seat`(
	`id` INT NOT NULL,
	`name` VARCHAR(255) ,
	`available_for_parking` VARCHAR(255) ,
	PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `flight_gate_apron_seat_connection`(
	`flight_id` INT NOT NULL,
	`gate_id` INT NOT NULL,
	`apron_seat_id` INT NOT NULL,
	`mode_of_connection` VARCHAR(255),
	PRIMARY KEY (`flight_id`,`gate_id`,`apron_seat_id`)
);

CREATE TABLE IF NOT EXISTS `employee_flight`(
	`flight_id` INT NOT NULL,
	`employee_id` INT NOT NULL,
	PRIMARY KEY (`flight_id`,`employee_id`)
);

ALTER TABLE `airline` ADD CONSTRAINT `airline_fk0` FOREIGN KEY (`com_id`) REFERENCES `company`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `aircharter` ADD CONSTRAINT `aircharter_fk0` FOREIGN KEY (`comp_id`) REFERENCES `company`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `freight_company` ADD CONSTRAINT `freight_company_fk0` FOREIGN KEY (`comp_id`) REFERENCES `company`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `passenger_company` ADD CONSTRAINT `passenger_company_fk0` FOREIGN KEY (`comp_id`) REFERENCES `company`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `employee` ADD CONSTRAINT `employee_fk0` FOREIGN KEY (`comp_id`) REFERENCES `company`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pilot` ADD CONSTRAINT `pilot_fk0` FOREIGN KEY (`id`) REFERENCES `employee`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `flight_attendant` ADD CONSTRAINT `flight_attendant_fk0` FOREIGN KEY (`id`) REFERENCES `employee`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `aircraft` ADD CONSTRAINT `aircraft_fk0` FOREIGN KEY (`comp_id`) REFERENCES `company`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `passenger_aircraft` ADD CONSTRAINT `passenger_aircraft_fk0` FOREIGN KEY (`id`) REFERENCES `aircraft`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `freight_aircraft` ADD CONSTRAINT `freight_aircraft_fk0` FOREIGN KEY (`id`) REFERENCES `aircraft`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `flight` ADD CONSTRAINT `flight_fk0` FOREIGN KEY (`comp_id`) REFERENCES `company`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `flight` ADD CONSTRAINT `flight_fk1` FOREIGN KEY (`aircraft_id`) REFERENCES `aircraft`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `domestic_flight` ADD CONSTRAINT `domestic_flight_fk0` FOREIGN KEY (`id`) REFERENCES `flight`(`id`)ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `international_flight` ADD CONSTRAINT `international_flight_fk0` FOREIGN KEY (`id`) REFERENCES `flight`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `intercontinental_flight` ADD CONSTRAINT `intercontinental_flight_fk0` FOREIGN KEY (`id`) REFERENCES `flight`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `arrival` ADD CONSTRAINT `arrival_fk0` FOREIGN KEY (`flight_id`) REFERENCES `flight`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `arrival` ADD CONSTRAINT `arrival_fk1` FOREIGN KEY (`runway_id`) REFERENCES `runway`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `departure` ADD CONSTRAINT `departure_fk0` FOREIGN KEY (`flight_id`) REFERENCES `flight`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `departure` ADD CONSTRAINT `departure_fk1` FOREIGN KEY (`runway_id`) REFERENCES `runway`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `flight_gate_apron_seat_connection` ADD CONSTRAINT `flight_gate_apron_seat_connection_fk0` FOREIGN KEY (`flight_id`) REFERENCES `flight`(`id`) 
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `flight_gate_apron_seat_connection` ADD CONSTRAINT `flight_gate_apron_seat_connection_fk1` FOREIGN KEY (`gate_id`) REFERENCES `gate`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `flight_gate_apron_seat_connection` ADD CONSTRAINT `flight_gate_apron_seat_connection_fk2` FOREIGN KEY (`apron_seat_id`) REFERENCES `apron_seat`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `employee_flight` ADD CONSTRAINT `employee_flight_fk0` FOREIGN KEY (`flight_id`) REFERENCES `flight`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `employee_flight` ADD CONSTRAINT `employee_flight_fk1` FOREIGN KEY (`employee_id`) REFERENCES `employee`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;


INSERT INTO company VALUES (1,'RYANAIR Ltd','FR','RYR','Ryanair','Ireland'),
(2,'Aegean Airlines S.A.','A3','AEE','Aegean','Greece'),
(3,'QATAR AIRWAYS','QR','QTR','Qatari','Qatar'),
(4,'TURKISH AIRLINES','TK','THY','Turk Air','Turkey'),
(5,'AMERICAN AIRLINES Inc.','AA','AAL','American','USA'),
(6,'AIR CHINA International Corp.','CA','CCA','AIR CHINA','China'),
(7,'BRITISH AIRWAYS Plc','BA','BAW','Speedbird','England'),
(8,'ASTRA AIRLINES','A2','AZI','GREEK STAR','Greece'),
(9,'DELTA AIR LINES Inc.','DL','DAL','Delta','USA'),
(10,'OLYMPIC AIR','OA','OAL','Olympic','Greece');

INSERT INTO airline VALUES(1),(2),(3),(4),(5),(6),(7),(8),(9),(10);

INSERT INTO passenger_company VALUES(1),(2),(3),(4),(5),(6),(7),(8),(9),(10);

INSERT INTO freight_company VALUES (4),(5),(10);

INSERT INTO aircharter VALUES (1),(2),(3),(10);

INSERT INTO employee VALUES(1,'PENELOPE','GUINESS','1987-02-15','F',10000,9),
(2,'NICK','WAHLBERG','1978-04-25','M',3000,9),
(3,'ED','CHASE','1979-04-11','M',11876,9),
(4,'JENNIFER','DAVIS','1990-09-07','F',1936,9),
(5,'JOHNNY','LOLLOBRIGIDA','1991-07-19','M',9874,9),
(6,'BETTE','NICHOLSON','1982-09-29','F',1000,1),
(7,'GRACE','MOSTEL','1983-12-06','F',1276,1),
(8,'MATTHEW','JOHANSSON','1986-04-15','M',983,1),
(9,'JOE','SWANK','1991-08-03','M',5970,1),
(10,'CHRISTIAN','GABLE','1984-04-15','M',3850,1),
(11,'ZERO','CAGE','1985-12-15','M',6920,2),
(12,'KARL','BERRY','1993-01-15','M',870,2),
(13,'UMA','WOOD','1988-02-15','F',900,2),
(14,'VIVIEN','BERGEN','1988-03-09','F',2000,2),
(15,'CUBA','OLIVIER','1985-06-15','M',850,2),
(16,'FRED','COSTNER','1990-11-17','M',8000,3),
(17,'HELEN','VOIGHT','1989-02-15','F',1700,3),
(18,'DAN','TORN','1986-05-23','M',1700,3),
(19,'BOB','FAWCETT','1982-03-04','M',10000,3),
(20,'LUCILLE','TRACY','1975-02-13','F',7500,3),
(21,'KIRSTEN','PALTROW','1982-10-21','M',1200,4),
(22,'ELVIS','MARX','1975-05-19','M',5000,4),
(23,'SANDRA','KILMER','1996-03-18','F',1000,4),
(24,'CAMERON','STREEP','1981-07-06','F',4000,4),
(25,'KEVIN','BLOOM','1985-06-15','M',1000,4),
(26,'RIP','CRAWFORD','1986-01-14','M',800,5),
(27,'JULIA','MCQUEEN','1990-12-11','F',900,5),
(28,'WOODY','HOFFMAN','1979-05-31','M',3000,5),
(29,'ALEC','WAYNE','1986-04-15','M',3200,5),
(30,'SANDRA','PECK','1980-09-18','F',3000,5),
(31,'SISSY','SOBIESKI','1996-02-28','F',650,6),
(32,'TIM','HACKMAN','1986-12-05','M',650,6),
(33,'MILLA','PECK','1984-08-15','F',2000,6),
(34,'AUDREY','OLIVIER','2006-02-15 04:34:33','F',2000,6),
(35,'JUDY','DEAN','1993-10-19','F',800,6),
(36,'BURT','DUKAKIS','1983-04-18','M',900,7),
(37,'VAL','BOLGER','1989-02-12','M',900,7),
(38,'TOM','MCKELLEN','1987-01-25','M',2000,7),
(39,'GOLDIE','BRODY','1986-07-16','F',2000,7),
(40,'JOHNNY','CAGE','1985-04-11','M',900,7),
(41,'JODIE','DEGENERES','1980-02-05','F',900,8),
(42,'TOM','MIRANDA','1986-08-15','M',900,8),
(43,'KIRK','JOVOVICH','1982-11-04','M',2000,8),
(44,'NICK','STALLONE','1979-02-18','M',3000,8),
(45,'REESE','KILMER','1996-02-15','M',900,8),
(46,'PARKER','GOLDBERG','1979-07-11','M',6000,10),
(47,'JULIA','BARRYMORE','1984-08-07','F',5000,10),
(48,'FRANCES','DAY-LEWIS','1993-03-14','F',1000,10),
(49,'ANNE','CRONYN','1993-11-15','F',1000,10),
(50,'NATALIE','HOPKINS','1996-04-15','F',1000,10);

INSERT INTO pilot VALUES(1,'A110'),(5,'A111'),(3,'A112'),(9,'A113'),(10,'A114'),(11,'A115'),(14,'A116'),(16,'A117'),(19,'A118'),(20,'A119'),(22,'120'),(24,'A121'),(28,'A122'),(29,'A123'),(30,'A124'),
(33,'A125'),(34,'A126'),(39,'A127'),(38,'A128'),(43,'A129'),(44,'A130'),(46,'A131'),(47,'A132');

INSERT INTO flight_attendant VALUES (2),(4),(6),(7),(8),(15),(13),(12),(17),(18),(23),(21),
(25),(26),(27),(31),(32),(35),(36),(37),(40),(41),(42),(45),(48),(49),(50);

INSERT INTO aircraft VALUES (1,'Airbus Industries','A320',145.505,138,111.30,'A320',39.10,'C-III',2),
(2,'Airbus Industries','A321',206.000,138,111.83,'A321',38.58,'C-III',2),
(3,'Airbus Industries','A321',206.000,138,111.83,'A321',38.58,'C-III',1),
(4,'Airbus Industries','A321',206.000,138,111.83,'A321',38.58,'C-III',3),
(5,'Airbus Industries','A320',145.505,138,111.30,'A320',39.10,'C-III',4),
(6,'Airbus Industries','A319',141.095,138,111.30,'A319',38.70,'C-III',4),
(7,'Airbus Industries','A319',141.095,138,111.30,'A319',38.70,'C-III',5),
(8,'Airbus Industries','A321',206.000,138,111.83,'A321',38.58,'C-III',6),
(9,'Airbus Industries','A320',145.505,138,111.30,'A320',39.10,'C-III',7),
(10,'Airbus Industries','A320',145.505,138,111.30,'A320',39.10,'C-III',8),
(11,'Airbus Industries','A321',206.000,138,111.83,'A321',38.58,'C-III',9),
(12,'De Havilland DHC-8','DASH 8-300',41.100,90,90.00,'DH8C',24.58,'A-III',10);

INSERT INTO passenger_aircraft VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12);

INSERT INTO flight VALUES (1,'A3540','Athens-ATH','Thessaloniki Makedonia','2016-06-01',2,1,'00:55'),
(2,'A3833','Frankfurt-Int Airport','Athens-ATH','2016-06-04',2,2,'02:55:00.0000000'),
(3,'FR171','Athens-ATH','Berlin-Schoenefeld-SXF','2016-06-04',1,3,'02:50:00'),
(4,'FR1243','Budapest Airport','Athens-ATH','2016-06-05',1,3,'02:00:00'),
(5,'QR021','Athens-ATH','Doha-DOH','2016-06-02',3,4,'04:20:00'),
(6,'QR0211','Doha-DOH','Athens-ATH','2016-05-30',3,4,'04:30:00'),
(7,'TK7994','Athens-ATH','AtaturkIstanbul-IST terminal1','2016-06-07',4,5,'01:20:00'),
(8,'TK1841','AtaturkIstanbul-IST terminal1','Athens-ATH','2016-05-31',4,6,'01:20:00'),
(9,'AA758','Athens-ATH','Philadelphia International-PHL','2016-06-04',5,7,'09:45:00'),
(10,'AA759','Philadelphia International-PHL','Athens-ATH','2016-06-01',5,7,'09:45:00'),
(11,'CA962','Athens-ATH','Munich-MUC','2016-05-31',6,8,'02:25:00'),
(12,'CA961','Munich-MUC','Athens-ATH','2016-06-03',6,8,'02:35:00'),
(13,'BA0631','Athens-ATH','Heathrow-LHR','2016-05-31',7,9,'04:05:00'),
(14,'BA0631','Heathrow-LHR','Athens-ATH','2016-06-03',7,9,'03:35:00'),
(15, 'A20308','Athens-ATH','Samos-SMI','2016-06-02',8,10,'01:00:00'),
(16, 'A20309','Samos-SMI','Athens-ATH','2016-06-07',8,10,'01:00:00'),
(17,'DL213','Athens-ATH','New York-JFK','2016-06-17',9,11,'10:50:00'),
(18,'DL214','New York-JFK','Athens-ATH','2016-06-30',9,11,'09:51:00'),
(19,'OA020','Athens-ATH','Astypalaia-JTY','2016-05-29',10,12,'01:05:00'),
(20,'OA021','Astypalaia-JTY','Athens-ATH','2016-06-06',10,12,'01:05:00');

INSERT INTO domestic_flight VALUES (1),(15),(16),(19),(20);

INSERT INTO international_flight VALUES (2),(3),(4),(11),(12),(13),(14);

INSERT INTO intercontinental_flight VALUES (5),(6),(7),(8),(9),(10),(17),(18);

INSERT INTO runway VALUES (1,'03R/21L',45,4000,400000),(2,'03L/21R',45,3800,300000);

INSERT INTO arrival VALUES (1,'2016-06-04','21:55:00','21:55:00','Arrived',2,1,'21L'),
(2,'2016-06-05','14:15','14:00','Late',4,1,'21L'),
(3,'2016-06-05','19:30','19:20','Expected',6,2,'03L'),
(4,'2016-06-05','21:20','21:20','Expected',8,2,'21R'),
(5,'2016-06-05','09:25','09:15','Arrived',10,1,'21L');

INSERT INTO departure VALUES (1,'2016-06-05','08:25','Departed',1,1,'21L'),
(2,'2016-06-05','17:20','Gate Closed',3,2,'21R'),
(3,'2016-06-05','13:40','Departed',5,1,'21L'),
(4,'2016-06-05','19:10','Gate Open',7,1,'03R'),
(5,'2016-06-05','18:40','Gate Open',13,2,'21R');

INSERT INTO gate VALUES (1,'G1'),(2,'G2'),(3,'G3'),(4,'G4'),(5,'G5'),(6,'G6'),(7,'G7'),(8,'G8'),(9,'G9'),(10,'G10');

INSERT INTO apron_seat VALUES (1,'AP1','Y'),(2,'AP2','N'),(3,'AP3','N'),(4,'AP4','N'),(5,'AP5','Y'),(6,'AP6','N'),(7,'AP7','N'),
(8,'AP8','N'),(9,'AP9','N'),(10,'AP10','N');

INSERT INTO flight_gate_apron_seat_connection VALUES(1,1,1,'ramp'),(13,9,9,'ramp'),(2,2,2,'bus'),(3,3,3,'ramp'),(4,4,4,'bus'),
(5,5,5,'bus'),(6,6,6,'ramp'),(7,7,7,'ramp'),(8,8,8,'ramp'),(10,10,10,'ramp');

INSERT INTO employee_flight VALUES(17,1),(17,2),(17,3),(17,4),(17,5),(18,1),(18,2),(18,3),(18,4),(18,5),
(3,6),(3,7),(3,8),(3,9),(3,10),(4,6),(4,7),(4,8),(4,9),(4,10),
(1,11),(1,12),(1,13),(1,14),(1,15),(2,11),(2,12),(2,13),(2,14),(2,15)
,(5,16),(5,17),(5,18),(5,19),(5,20),(6,16),(6,17),(6,18),(6,19),(6,20),
(7,21),(7,22),(7,23),(7,24),(7,25),(8,21),(8,22),(8,23),(8,24),(8,25),
(9,26),(9,27),(9,28),(9,29),(9,30),(10,26),(10,27),(10,28),(10,29),(10,30),
(11,31),(11,32),(11,33),(11,34),(11,35),(12,31),(12,32),(12,33),(12,34),(12,35),
(13,36),(13,37),(13,38),(13,39),(13,40),(14,36),(14,37),(14,38),(14,39),(14,40),
(15,41),(15,42),(15,43),(15,44),(15,45),(16,41),(16,42),(16,43),(16,44),(16,45),
(19,46),(19,47),(19,48),(19,49),(19,50),(20,46),(20,47),(20,48),(20,49),(20,50);
